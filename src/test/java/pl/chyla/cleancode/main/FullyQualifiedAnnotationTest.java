package pl.chyla.cleancode.main;

import org.hamcrest.CoreMatchers;
import org.hamcrest.Matcher;
import org.junit.Test;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static org.junit.Assert.*;

public class FullyQualifiedAnnotationTest {

    @Test
    public void simple() throws InterruptedException, ExecutionException, TimeoutException {
        var f = new FullyQualifiedAnnotation();
        var v = f.calculateValue(0).get(500, TimeUnit.MILLISECONDS);
        assertThat(v, CoreMatchers.equalTo(10));
    }

}