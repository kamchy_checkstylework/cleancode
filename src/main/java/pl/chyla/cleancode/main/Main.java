package pl.chyla.cleancode.main;

public final class Main {
    public static void main(String[] args) {
        new FullyQualifiedAnnotation().start();
        new ImportedAnnotation().run();
    }

}
