package pl.chyla.cleancode.main;

import pl.chyla.cleancode.anno.AnnoRed;

@AnnoRed
class ImportedAnnotation {
    void run() {
        System.out.println("ImportedAnnotation run");
    }
}
