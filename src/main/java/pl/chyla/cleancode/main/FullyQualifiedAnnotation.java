package pl.chyla.cleancode.main;

import java.util.concurrent.CompletableFuture;

@pl.chyla.cleancode.anno.AnnoRed
class FullyQualifiedAnnotation {
    void start() {
        System.out.println("FullyQualifiedAnnotation started");
    }

    public CompletableFuture<Integer> calculateValue(int a) {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return CompletableFuture.completedFuture(a + 10);
    }
}
